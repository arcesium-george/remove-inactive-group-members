# Remove inactive members from GitLab.com group

Recursively remove all inactive users from a GitLab.com group, its subgroups and projects using the GitLab API. Can be run as scheduled pipeline to continuously remove inactive users.

## Configuration

- Export / fork repository.
- Add a GitLab **group owner** API token to CI/CD variables named `GITLAB_TOKEN`. Make sure it is "masked".
This token will be used to query the API for group and project members.
The token **must** be an group owner token to be able to remove members of the group them.
- Add your GitLab instance URL to CI/CD variables named `GITLAB_URL`.
- notice the Job is tagged "local" for my testing purposes. Make sure your runners pick it up.
- Schedule the pipeline to run it weekly or your desired interval, using Pipelines -> Schedules
- Upvote this issue: https://gitlab.com/gitlab-org/gitlab/-/issues/211754

## Usage

`python3 remove-inactive-group-members.py $GITLAB_URL $GITLAB_TOKEN $GROUP_ID $INACTIVITY_DAYS --dryrun`

I recommend running this manually and doing a dryrun first

## Parameters

- `$GITLAB_URL`: URL of the GitLab instance
- `$GITLAB_TOKEN`: Group owner API token
- `$GROUP_ID`: ID or namespace of the group this script should run on
- `$INACTIVITY_DAYS`: Number of days an account can be inactive before it will be removed from the group and projects
- `--dryrun`: Dryrun mode. Only compile the CSV report of users to remove, don't actually remove anyone.

## What does it do?

* Request all subgroups and projects of a group
* Get all members of the group, its subgroups and projects
  * don't get the top group owners and the owner of the API token to not lock yourself out of the group
* For all members:
  * Query the [user events API](https://docs.gitlab.com/ee/api/events.html#get-user-contribution-events) and get the last contribution made.
  * if the days since that contribution are more than specified in $INACTIVITY_DAYS, i.e. the user has not contributed for more days than specified, remove the user from all groups and projects they are a direct member of
* For all members to be removed:
  * Add them to a CSV report
